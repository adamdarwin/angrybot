let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let json2csv = require('json2csv').parse;
let fs = require('fs');
let path = require('path');


let userSchema = new Schema({
    username    :   String,
    email       :   String,
    phone       :   String,
    address     :   String
});

let User = mongoose.model('users', userSchema);

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

exports.setUser = async (search, username) =>{
    try {
        let find = await findUser(search, username);
        if(!find){
            let user  = new User({
                username    :   username,
                email       :   '',
                phone       :   '',
                address     :   '',
                url         :   ''
            });
            let data = await search.getContact(username);
            while(data === 1){
                await sleep(20000);
                data = await search.getContact(username);
            }
            if(data){
                user.email = data.email;
                user.phone = data.phone;
                if(data.email){user.email = data.email;}
                if(data.phone){user.phone = data.phone;}
                if(data.address){user.address = data.address;}
                user.url = data.url;
                await user.save(function(err){
                    if(err) console.log('user.save function error2: ',err);
                });
                return user;
            }
        }
        await search.countUpdate('invalid');
        return false;
    }catch (e) {
        console.error('setUser = async (username): ', e);
    }
    return false;
};
const findUser = async (search, username) =>{
    try {
        let found;
        await User.find({ username :   username.toString()}, async function (err, doc) {
            if(!err){
                if(!doc[0]){
                    found = false;
                }
                else{
                    await search.countUpdate('repeat');
                    found = true;
                }
            }
            else{
                console.log('Err', err);
            }
        });

        // console.log('findUsers found', typeof found);
        return found;
    }catch (e) {
        console.error('findUser = async (username): ', e);
    }
    return false;
};

exports.getDump = async ()=>{
    try {
        let fields = ['username', 'email', 'phone', 'address'];
        const dateTime = new Date().toISOString().slice(-24).replace(/\D/g,'').slice(0, 14);
        const filePath = path.join(__dirname).slice(0, -7)+'/dumps/csv-'+dateTime + ".csv";
        await User.find({}).then((users)=>{
            if(users){
                let csv;
                try {
                    csv = json2csv(users, { fields });
                } catch (err) {
                    console.log(err);
                }
                console.log('csv ready: ', filePath);
                fs.writeFile(filePath, csv, function (err) {
                    if (err) {
                        console.log(err);
                    }else{
                        console.log('Dump Created');
                    }
                });
            }
        });
        return filePath;
    }catch (e) {
        console.log('getDump ERROR: ', e);
    }
};