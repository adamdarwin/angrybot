const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const UserSchema = new mongoose.Schema({
    username    :   String,
    password    :   String,
    status      :   Number
});

const Users = mongoose.model('profiles', UserSchema);

exports.getUser = async (username, password)=>{
    try{
        let user=false;
        const saltRounds = 10;
        await Users.findOne({username: username}).then((doc)=>{
            if(doc){
                if(!bcrypt.compareSync(password, doc.password)){ user = false; }
                else{
                    user = {};
                    user.username = doc.username;
                    user.status = doc.status;
                }
            }
        });
        return user;
    }catch (e) {
        console.log('can\'t get user: ', e);
    }
    return false;
};

exports.setUser = async (username, password)=>{
    try{
        let user = false;
        await Users.findOne({username: username}).then(async (doc)=>{
            if(doc){
                console.log('User already exist');
            }else{
                const saltRounds = 10;
                let salt = bcrypt.genSaltSync(saltRounds);
                let hash = bcrypt.hashSync(password, salt);
                user = new Users({
                    username: username,
                    password: hash
                });
                await user.save((e)=>{
                    if(e)
                        console.log(e);
                });
            }
        });
        return user.username;
    }catch (e) {
        console.log(e);
    }
};