let express = require('express');
let router = new express.Router();

let Users = require('.././models/Profiles');

router.get('/register', (req, res, next)=>{
    res.render('register',{title: 'Register'});
});
router.post('/register', async (req, res, next)=>{
    if(await Users.setUser(req.body.username, req.body.password)){
        req.logout();
        res.redirect('/');
    }
});


module.exports = router;