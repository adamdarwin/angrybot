let express = require('express');
let router = express.Router();
let passport = require('passport');


/* Get login page */
router.get('/login', function (req, res) {
    res.render('login', { title: 'Login'});
});

router.post('/login', function (req, res, next) {
    passport.authenticate('local', {
        successRedirect: '/main',
        failureRedirect: '/'
    })(req, res, next);
});

module.exports = router;