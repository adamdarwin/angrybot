let LocalStrategy = require('passport-local');

const Users = require('.././models/Profiles');
module.exports = function(passport){
    passport.use(new LocalStrategy(async (username, password, done)=>{
        let user = await Users.getUser(username, password);
        console.log(user);
        if(!user){
            console.log('Invalid data');
            return done(null, false)
        }else{
            console.log(user);
            return done(null, user);
        }
    }));
    passport.serializeUser(function(user, done) {
        done(null, user);
    });

    passport.deserializeUser(function(user, done) {
        done(null, user);
    });
};

