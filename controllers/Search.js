let curlEnt = require( 'axios' );
let md5 = require('md5');
let zip = require('zip-local');
let fs = require('fs');


const url = 'https://www.instagram.com/';
let  Users = require('.././models/Users');
let backup = require('mongodb-backup');


exports.main = async (socket, hashtags)=>{
    console.log('main');
    let search = new Search(socket);
    try{
        search.start(hashtags);
        return search;
    }catch (e) {
        console.log("Can't start search (main): ", e);
    }
    return false;
};

escapeUnicode = (str) => {
    return str.replace(/[^\0-~]/g, function(ch) {
        return "\\u" + ("0000" + ch.charCodeAt().toString(16)).slice(-4);
    });
};

exports.download = async (res)=>{
    let path = await Users.getDump();
    if(path){
        zip.zip(path, (e, zipped)=>{
            if(!e){
                zipped.compress();
                zipped.save(path+'.zip', function(error) {
                    if(!error) {
                        res.download(path+'.zip');
                    }
                });
            }
            else{
                console.log(e);
            }
        });
        // setTimeout(function () {
        //     fs.unlinkSync(path); // delete this file after 30 seconds
        //     fs.unlinkSync(path+'.zip'); // delete this file after 30 seconds
        // }, 30000);
    }
};

class Search{
    constructor(socket){
        this.stream = 0;
        this.status = false;
        this.searchData = {
            user : ''   // User string for client output.
        };
        this.countData = {
            count       :   0,  // Total count of found users.
            invalid     :   0,  // Count of users, that haven't been saved to base.
            unique      :   0,  // Count of user, that have been saved to base.
            repeat      :   0   // Count of users, that already been saved to base.
        };
        this.initialData = {
            queryHash   :   'f92f56d47dc7a55b606908374b43a314',
            hashtag     :   'hair',
            show_ranked :    false,
            first       :   100,
            rhx         :   ''
        };
        this.socket = socket;
    }

    async start(hashtags = false){
        try{
            if(hashtags){
                this.initialData.hashtag = hashtags;
            }
            console.log('Search by: ', this.initialData.hashtag);
            this.statusChange();
            this.searchData.user = '';
            this.socket.emit('update', this.searchData);
            this.countUpdate();
            let body = await this.getBody(this.initialData.hashtag);
            this.searchEngine(body);
            this.scrollEngine(body.after);
        }catch (e) {
            console.log("Can't start search (start): ", e);
        }
    }

    statusChange(){
        this.status = !(this.status);
    }

    countUpdate(count = false){
        if(!count){
            Object.keys(this.countData).map((key) =>{
                this.countData[key] = 0;
            });
        }else{
            this.countData[count]++;
        }
        this.socket.emit('countUpdate', this.countData);
    }

    async getBody(hashtag){
        try{
            const curl = curlEnt; //new curlEnt();
            const config = {
                headers: {
                    'user-agent' : 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36'
                }
            };

            let resp = await curl.get(url+'explore/tags/'+hashtag+'/', config);

            let after = '';
            let rhx = '';
            //getting "after" param
            //TODO: create checkup if 'end_cursor' field not found
            let posE = resp.data.indexOf('end_cursor')+13;
            let k=0;
            while(resp.data[posE+k] !== '=' || k>1000){

                if(!this.status)return false;

                after+= resp.data[posE+k];
                k++;
            }

            let posR = resp.data.indexOf('rhx_gis')+10;
            //getting rhx_gis from first request
            k=0;
            while(resp.data[posR+k] !== '"' || k>1000){

                if(!this.status)return false;

                rhx+= resp.data[posR+k];
                k++;
            }

            this.initialData.rhx = rhx;
            let shortcodes = [];
            let pos = 0;
            k=0;
            while(true) {
                if(!this.status)return false;
                let index = resp.data.indexOf('"shortcode":', pos);
                let code ='';
                let i =0 ;
                while(resp.data[index+i+13] !== '"' || i>1000){

                    if(!this.status)return false;

                    code+= resp.data[index+i+13];
                    i++;
                }
                if (index < 0 || k>1000) break;
                shortcodes.push(code);
                pos = index+1;
                k++;
            }
            let initResp = {};
            initResp.shortcodes = shortcodes;
            initResp.after = after;
            if(!initResp.shortcodes || !initResp.after)
                console.log('getBody SOMETHING BAD');
            return initResp;
        }catch (e) {
            console.log("Can't get initial body (getBody): ", e);
        }
        return false;
    }

    async searchEngine(body){
        try{
            if(!this.status)return false;
            let shortcodes = body.shortcodes;
            if(shortcodes){
                await this.getUsername(shortcodes);
                console.log('Search ends');
                this.stream--;
            }else{
                console.log("Can't get shortcodes. Search ends.");
                this.stream--;
            }
        }catch(e){
            console.log('searchEngine crushed: ');
        }
        return false;
    }

    async scrollEngine(after){
        try{
            if(!this.status)return true;
            console.log('Try to create stream, current: ', this.stream);
            if(this.stream <5 && after){
                let scrollTemp = await this.getScroll(after);
                if(scrollTemp){
                    this.scroll = scrollTemp;
                    this.stream++;
                    console.log('Stream created');
                    console.log('Streams: ',this.stream);
                    try{
                        this.searchEngine(this.scroll);
                    }catch (e) {
                        console.error('scrollEngine function error1: ');
                    }
                    try {
                        this.scrollEngine(this.scroll.after);
                    }catch (e) {
                        console.error('scrollEngine function error2: ');
                    }
                }
                else if(this.status){
                    console.log('Scroll is empty. Oops');
                    scrollTemp = await this.getScroll(after);
                    console.log('2-ND ATTEMPT: ', scrollTemp);
                }
            }else{
                setTimeout(()=>{
                    this.scrollEngine(after);
                }, 10000);
            }
        }catch (e) {
            console.log('scrollEngine error: ');
        }
    }

    async getScroll(after){
        try{
            let variables = '{"tag_name":"' + this.initialData.hashtag + '","show_ranked":'+ this.initialData.show_ranked +',"first":'+ this.initialData.first +',"after":"'+ after +'=="}';
            let gis = md5(this.initialData.rhx+':'+variables.toString());
            //const curl = new curlEnt();
            const curl = curlEnt; //new curlEnt();
            const config = {
                headers: {
                    'user-agent' : 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
                    'x-instagram-gis': gis
                }
            };
            let resp = false;
            resp = await curl.get(url+'graphql/query/?query_hash='+this.initialData.queryHash+'&variables=%7B%22tag_name%22%3A%22'+this.initialData.hashtag+'%22%2C%22show_ranked%22%3A'+this.initialData.show_ranked+'%2C%22first%22%3A'+this.initialData.first+'%2C%22after%22%3A%22'+after+'%3D%3D%22%7D', config).catch((e)=>{
                console.log('getScroll try to get resp ERR: ');
            });
            if(resp.status = 200){
                resp = resp.data.data.hashtag.edge_hashtag_to_media;
                let shortcodes = [];
                for(let i = 0; i<resp.edges.length; i++){

                    if(!this.status)return false;

                    shortcodes[i] = resp.edges[i].node.shortcode;
                }
                let data = {};
                data.after = resp.page_info.end_cursor;
                data.after = data.after.substr(0, data.after.length -2);
                data.shortcodes = shortcodes;
                return data;
            }
            else{
                console.log("Can't get scroll");
                return false;
            }
        }catch (e) {
            console.log('getScroll = async (after): ');
        }
        return false;
    }

    async getUsername(shortcodes){
        try {
            const sLength = shortcodes.length;
            const curl = curlEnt; //new curlEnt();
            const config = {
                headers: {
                    'user-agent' : 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
                }
            };
            for(let i =0 ; i < sLength ; i++){

                if(!this.status)return false;

                let resp = false;
                resp = await curl.get(url+'p/'+shortcodes[i]+'/', config).catch((e)=>{
                    console.log('Tried to get resp getUsername ERROR: ');
                });
                // console.log('getUsername resp', typeof resp);

                if(resp){
                    let pos = resp.data.indexOf('<link rel="canonical" href="https://www.instagram.com/')+54;

                    let username = '';
                    let k=0;
                    while(resp.data[pos+k] !== '/' || k>100){

                        if(!this.status)return false;

                        username += resp.data[pos+k];
                        k++;
                    }
                    if(username === 'p'){
                        username = '';
                        pos = resp.data.indexOf('"author:{"');
                        k = 0;
                        if(pos !== -1){
                            pos = resp.data.indexOf('"alternateName":"@', pos)+18;
                            while(resp.data[pos+k] !== '"' || k>100){
                                if(!this.status)return false;

                                username += resp.data[pos+k];
                                k++;
                            }
                        }else{
                            pos = resp.data.indexOf('"owner":{');
                            pos = resp.data.indexOf('"username":"')+12;
                            while(resp.data[pos+k] !== '"' || k>100){
                                if(!this.status)return false;

                                username += resp.data[pos+k];
                                k++;
                            }
                        }
                    }

                    let user = await Users.setUser(this, username);
                    if(user){
                        this.searchData.unique++;
                        this.searchData.user = user.username+':  '+user.email+'   '+user.phone+'   '+user.address+'     '+user.url;
                        this.socket.emit('update', this.searchData);
                        await this.countUpdate('unique');
                    }
                    await this.countUpdate('count');
                }
            }
            console.log('Done00');
            return true;
        }catch (e) {
            console.error('getUsername Error');
        }
        return false;
    }

    async getContact(username){
        try {
            let data = {
                email   :   '',
                phone   :   '',
                address :   '',
                url     :   ''
            };
            const curl = curlEnt; //new curlEnt();
            const config = {
                headers: {
                    'user-agent' : 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
                }
            };

            let resp = await curl.get(url+username+'/', config, (e)=>{
                if(e){
                    console.log(e);
                }
            });
            if(resp){
                let pos = resp.data.indexOf('"is_business_account"');
                // console.log('still here 2');

                if(pos !== -1 && resp.data[pos+22] !== 'f'){
                    pos = resp.data.indexOf('"business_email":"')+18;
                    if(resp.data[pos] !== '"'){
                        let k=0;
                        while(resp.data[pos+k] !== '"'){

                            if(!this.status)return false;

                            data.email += resp.data[pos+k];
                            k++;
                        }
                    }
                    pos = resp.data.indexOf('"business_phone_number":"')+25;
                    if(resp.data[pos] !== '"'){
                        let k=0;
                        while (resp.data[pos+k] !== '"'){

                            if(!this.status)return false;

                            data.phone += resp.data[pos+k];
                            k++;
                        }
                    }
                    pos = resp.data.indexOf('"business_address_json":"')+25;
                    if(resp.data[pos] !== '"'){
                        let k=0;
                        while(resp.data[pos+k] !== '}'){

                            if(!this.status)return false;

                            if(resp.data[pos+k] !== '\\')
                                data.address += resp.data[pos+k];
                            k++;
                        }
                        data.address += resp.data[pos+k];
                        //TODO: make an empty address check

                        // let address = JSON.parse(this.escapeUnicode(data.address));
                        // if(address.street_address || address.zip_code || address.city_name || address.region_name || address.country_code){
                        //     data.address = JSON.stringify(address);
                        // }else{
                        //     data.address = false;
                        // }
                        data.url = url+username+'/';
                    }
                    if(!data.email &&  !data.phone && !data.address){
                        await this.countUpdate('invalid');
                        return false;
                    }
                    // console.log('getContact returns data', typeof data);
                    return data;
                }
                return 2;
            }
        }catch (e) {
            console.log('Contact timeout', username);
        }
        return 1;
    }
}


