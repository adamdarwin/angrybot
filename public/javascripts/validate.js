window.load = ()=>{
    const validate = ()=>{
        let username = document.getElementById('username');
        let password = document.getElementById('password');
        let rPassword = document.getElementById('username');
        if(username.length() > 30){
            console.log('Incorrect username');
            return false;
        }
        if(typeof username[0] === 'number'){
            console.log('Should start from letter');
        }
        if(username !== escapeUnicode(username)){
            console.log('Username can\'t exist unicode');
            return false
        }
        if(password.length() < 8){
            console.log('Password too short');
            return false;
        }
        if(password.length() > 30){
            console.log('Password too big');
            return false;
        }
        if(password !== escapeUnicode(password)){
            console.log('Incorrect username');
            return false;
        }
        if(password !== rPassword){
            console.log('Passwords do not match');
            return false;
        }
    };

    const escapeUnicode = (str) => {
        return str.replace(/[^\0-~]/g, function(ch) {
            return "\\u" + ("0000" + ch.charCodeAt().toString(16)).slice(-4);
        });
    };
};