// let socket = io.connect('http://localhost:3000');
let socket = io.connect('http://116.203.101.118:3000');


window.onload = function () {
    let count = document.getElementById('count'),
        invalid = document.getElementById('invalid'),
        unique = document.getElementById('unique'),
        repeat = document.getElementById('repeat'),
        currentStatus = document.getElementById('currentStatus'),
        queryOutput = document.getElementById('queryOutput'),
        btnStartHashtag = document.getElementById('btnStartHashtag'),
        btnStartUser = document.getElementById('btnStartUser'),
        tags = document.getElementById('tags'),
        download = document.getElementById('download');

    socket.on('countUpdate', (data) =>{
        count.innerText = 'Count: '+data.count;
        invalid.innerText = 'Invalid: '+data.invalid;
        unique.innerText = 'Unique: '+data.unique;
        repeat.innerText = 'Repeat: '+data.repeat;
    });

    socket.on('update', function (data) {
        if(data.username !== '')
            queryOutput.value += data.user + '\n'+'--------------------------------------------\n';
        else {
            queryOutput.value = '';
        }
    });
    let status = false;
    btnStartHashtag.addEventListener('click', ()=>{
        if(tags.value){
            if(!status){
                btnStartHashtag.innerText = 'Stop';
                btnStartHashtag.classList.remove("start");
                btnStartHashtag.classList.add("stop");
                document.getElementById('tags').readOnly = true;
                console.log(tags.value);
                socket.emit('switch', tags.value);
                status = true;
                queryOutput.value = '';
            }
            else{
                socket.emit('switch');
                status = false;
                btnStartHashtag.innerText = 'Start';
                btnStartHashtag.classList.remove("stop");
                btnStartHashtag.classList.add("start");
                document.getElementById('tags').removeAttribute('readonly');
            }
        }else{
            alert('Enter hashtag please.');
        }
    });

    download.addEventListener('click', ()=>{
        // socket.emit('download');
        location.href = '/download';
    });

};