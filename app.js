let express = require('express');
let path = require('path');
let socket = require('socket.io');
let favicon = require('serve-favicon');
const session = require('express-session');
let mongoose = require('mongoose');
let passport = require('passport');

mongoose.connect('mongodb://admin:qwerty1@ds113586.mlab.com:13586/insta-parse', {useNewUrlParser: true});

require('./config/passport')(passport);


let searchController = require('./controllers/Search');

let app = express();

app.use(express.urlencoded({ extended: false }));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');


app.use(session({
    secret: 'asdhfaher'
}));
app.use(passport.initialize());
app.use(passport.session());


const auth =(req, res, next)=>{
    if(req.user){
        console.log(req.user.status);
        next()
    }else{
        return res.redirect('/login');
    }
};

app.use('/', require('./routes/index'));
app.all('/login', require('./routes/login.js'));
app.all('/main', auth, require('./routes/main.js'));
app.all('/register', auth, require('./routes/register.js'));
app.get('/logout', auth, (req, res)=>{
    req.logout();
    res.redirect('/');
});
app.use(express.static(path.join(__dirname, 'public')));
app.use(favicon(__dirname + '/public/images/favicon.ico'));

app.get('/download', async (req,res)=>{
    await searchController.download(res);
});

const PORT = process.env.PORT || 3000;

let server = app.listen(PORT, function () {
    console.log('Server up. Listening on '+PORT + ' port');
});

module.exports = app;

io = socket(server);
io.on('connection',  (socket) => {
    let search = false;
    console.log('Connected');
    socket.on('switch', (hashtags)=>{
        if(!search){
            searchController.main(socket, hashtags).then((obj)=>{
                search = obj;
            });
            socket.search = search;
        }else{
            search.statusChange();
            console.log('Stopped by client');
            search = false;
        }
    });
});